import './NewCost.css'
import CostForm from "./CostForm";
import ButtonAddNewCost from "./ButtonAddNewCost";
import React, {useState} from "react";

function NewCost(props) {
    const [showButton, setShowButton] = useState('True')

    let showContent = ''
    if (showButton === 'True') {
        showContent = <ButtonAddNewCost click={showCostFormHandler}/>
    } else {
        showContent = <CostForm onSaveCostDate={saveCostDateHandler} text='Добавить Расход'/>
    }

    function showCostFormHandler(){
        setShowButton('')
    }

    function saveCostDateHandler(inputCostData) {
        const costData = {
            ...inputCostData,
            id: Math.random().toString()
        }
        props.onAddCost(costData)
        setShowButton('True')
    }

    return (
        <div className='new-cost'>
            {showContent}
        </div>
    )
}

export default NewCost