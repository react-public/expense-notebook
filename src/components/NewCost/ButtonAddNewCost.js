import './ButtonAddNewCost.css'
function ButtonAddNewCost(props) {
    return(
        <button className='new-cost__button' onClick={props.click}>Добавить новый расход</button>
    )
}

export default ButtonAddNewCost