import './CostForm.css'
import React, {useState} from "react";

function CostForm(props) {
    const [name, setName] = useState('');
    const [amount, setAmount] = useState('');
    const [date, setDate] = useState('');

    function nameChangeHandler(event) {
        setName(event.target.value)
    }

    function amountChangeHandler(event) {
        setAmount(event.target.value)
    }

    function dateChangeHandler(event) {
        setDate(event.target.value)
    }

    function submitHandler(event) {
        event.preventDefault();
        let costData = {
            title: name,
            amount: amount,
            date: new Date(date)
        };
        if(props.id !== undefined) {
            costData.id = props.id;
        }
        props.onSaveCostDate(costData);
        setName('');
        setAmount('');
        setDate('');
    }

    return (
        <form onSubmit={submitHandler}>
            <div className='new-cost__control'>
                <div className='new-cost__control'>
                    <label>Название</label>
                    <input type="text"
                           onChange={nameChangeHandler}
                           value={name}
                    />
                </div>
                <div className='new-cost__control'>
                    <label>Сумма</label>
                    <input type="number"
                           min="0.01"
                           step="0.01"
                           onChange={amountChangeHandler}
                           value={amount}
                    />
                </div>
                <div className='new-cost__control'>
                    <label>Дата</label>
                    <input type="date"
                           min="2019-01-01"
                           step="2022-12-31"
                           onChange={dateChangeHandler}
                           value={date}
                    />
                </div>
                <div className='new-cost__actions'>
                    <button type="submit">{props.text}</button>
                </div>
            </div>
        </form>
    )
}


export default CostForm;