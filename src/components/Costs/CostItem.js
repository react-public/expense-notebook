import Popup from 'reactjs-popup';
import React, {useRef} from 'react';
import 'reactjs-popup/dist/index.css';
import './CostItem.css';
import CostDate from "./CostDate";
import Card from "../UI/Card";
import CostForm from "../NewCost/CostForm";


function CostItem(props) {
    const ref = useRef();
    const closeTooltip = () => ref.current.close();

    function saveCostDateHandler(inputCostData) {
        if (inputCostData !== undefined) {
            props.editCost(inputCostData)
            closeTooltip()
        }
    }

    return (
        <li>

            <Card className='cost-item'>
                <CostDate date={props.date}/>
                <div className='cost-item__description'>
                    <h2>{props.title}</h2>
                    <div className='cost-item__price'>${props.amount}</div>

                    <Popup ref={ref} trigger={
                        <button className='cost-item__button'>
                            Изменить
                        </button>
                    } position="right center">
                        <div className='cost-form__change'>
                            <CostForm text='Изменить Расход' onSaveCostDate={saveCostDateHandler} id={props.id}/>
                        </div>
                    </Popup>
                </div>
            </Card>

        </li>
    );
}

export default CostItem;