import "./CostsFilter.css";
import CostsFilterOption from "./CostsFilterOption";

const CostsFilter = (props) => {
    function yearChangeHandler(event) {
        props.onChangeYear(event.target.value);
    }

    return (
        <div className="costs-filter">
            <div className="costs-filter__control">
                <label>Выбор По Году</label>
                <select value={props.year} onChange={yearChangeHandler}>
                    {props.listYars.map((year) =>
                        <CostsFilterOption key={year.key} year={year.year}/>
                    )}
                </select>
            </div>
        </div>
    );
};

export default CostsFilter;