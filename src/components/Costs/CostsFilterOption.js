function CostsFilterOption(props) {
    return (
        <option value={props.year}>{props.year}</option>
    )
}

export default CostsFilterOption;