import CostItem from "./CostItem";
import './CostsList.css'
import React from "react";

function CostsList(props) {
    if (props.costs.length === 0) {
        return <h2 className='cost-list__fallback'>В этом году расходов нет</h2>
    }
    return (
        <ul className='cost-list'>
            {props.costs.map((cost) =>
                <CostItem
                    key={cost.id}
                    id={cost.id}
                    date={cost.date}
                    title={cost.title}
                    amount={cost.amount}
                    editCost={props.editCost}
                />
            )}
        </ul>
    )
}

export default CostsList