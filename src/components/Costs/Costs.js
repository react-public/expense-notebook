import './Costs.css';
import Card from "../UI/Card";
import CostsFilter from "./CostsFilter";
import React, {useState} from "react";
import CostsList from "./CostsList";
import CostsDiagram from "./CostsDiagram";

function Costs(props) {
    const [selectedYear, setSelectedYear] = useState(new Date().getFullYear().toString());
    const filteredCosts = props.costs.filter(cost => {
        return cost.date.getFullYear().toString() === selectedYear
    })
    let listYears = [];
    props.costs.forEach((cost) => {
        let year = cost.date.getFullYear().toString();
        if (!listYears.includes(year)) {
            listYears.push(year)
        }
    })
    listYears.sort(sortYears);
    listYears = addKey(listYears);

    function sortYears(a, b) {
        //по убыванию
        if (a > b) return -1;
        if (a === b) return 0;
        if (a < b) return 1;
    }

    function addKey(list) {
        //добавим ориг ключи для вывода
        let newList = [];
        list.forEach((element, index) => {
            let obj = {
                key: index,
                year: element
            };
            newList.push(obj);
        })
        return newList;
    }

    function selectYearHandler(year) {
        setSelectedYear(year)
    }

    return (
        <Card className='costs'>
            {props.costs.length === 0 && <p>Расходов нет</p>}
            {props.costs.length > 0 &&
                <CostsFilter year={selectedYear} listYars={listYears} onChangeYear={selectYearHandler}/>
            }
            {props.costs.length > 0 &&
                <CostsDiagram costs={filteredCosts}/>
            }
            <CostsList costs={filteredCosts} editCost={props.editCost}/>
        </Card>
    );
}

export default Costs;