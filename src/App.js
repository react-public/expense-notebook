import Costs from "./components/Costs/Costs";
import NewCost from "./components/NewCost/NewCost";
import React, {useState} from "react";
import ButtonAddNewCost from "./components/NewCost/ButtonAddNewCost";


const INITIAL_COSTS = [
    {
        id: 1,
        date: new Date(2023, 2, 12),
        title: 'Холодильник',
        amount: 999.99,
    },
    {
        id: 2,
        date: new Date(2022, 1, 25),
        title: 'Игры',
        amount: 600.99,
    },
    {
        id: 3,
        date: new Date(2021, 5, 23),
        title: 'Одежда',
        amount: 1500.99,
    },
]

function App() {
    const [costs, setCosts] = useState(INITIAL_COSTS);

    function addCostHandler(cost) {
        setCosts((prevState) => {
            return [cost, ...prevState]
        })
    }

    function editCostHandler(cost){
        setCosts((prevState) => {
            prevState.map(function (item) {
                if (item.id === cost.id) {
                    item.title = cost.title
                    item.amount = cost.amount
                    item.date = cost.date
                    return item
                } else {
                    return null
                }
            })
            return [...prevState]
        })
    }

    return (
        <div>
            <NewCost onAddCost={addCostHandler}/>
            <Costs costs={costs} editCost={editCostHandler}/>
        </div>
    );
}

export default App;
